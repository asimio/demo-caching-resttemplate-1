# README #

Accompanying source code for blog entry at http://tech.asimio.net/2017/07/14/Caching-using-RestTemplate-Ehcache-and-ETags.html.

Used in conjunction with https://bitbucket.org/asimio/demo-caching-resttemplate-2

### Requirements ###

* Java 8
* Maven 3.3.x

### Building and from command line ###

```
mvn clean package
```

### Running the application ###

First run ```demo-caching-resttemplate-2``` as described at https://bitbucket.org/asimio/demo-caching-resttemplate-2.

```
java -jar target/demo-caching-resttemplate-1.jar
```

### Available endpoints ###

```
curl -v http://localhost:8090/api/hello/{name}
```

### See also ###

https://bitbucket.org/asimio/demo-caching-resttemplate-2

### Who do I talk to? ###

* ootero at asimio dot net
* https://www.linkedin.com/in/ootero